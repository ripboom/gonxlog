# Sample Golang #
``` golang
package main

import (
	"gitlab.com/ripboom/gonxlog"
	"unsafe"
	"C"
)

//export onExtCall
func onExtCall(context unsafe.Pointer, ld unsafe.Pointer) {

	data := gonxlog.NxLogdataConv(ld)
	gonxlog.LogInfo("Hi man!")
	data.Set("where", "It's me")

}


func main() {

}
```