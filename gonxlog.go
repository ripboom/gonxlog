package gonxlog

/*
#cgo linux LDFLAGS: -Wl,--unresolved-symbols=ignore-all
#cgo darwin LDFLAGS: -Wl,-undefined,suppress -flat_namespace
#include <stdlib.h>

struct nx_go_ctx_;
typedef struct nx_go_ctx_ nx_go_ctx_t;

struct nx_go_logdata_;
typedef struct nx_go_logdata_ nx_go_logdata_t;

typedef struct
{
    char ** elts;
    int nelts;
} nx_go_str_array_t;



nx_go_logdata_t * nx_go_logdata_new();
void nx_go_logdata_free(nx_go_logdata_t * ld);
void nx_go_logdata_post(nx_go_ctx_t * ctx, nx_go_logdata_t * data);


char * nx_go_logdata_get(nx_go_logdata_t * ld, const char * field);
void nx_go_logdata_set(nx_go_logdata_t * ld, const char * field, const char * val);
void nx_go_logdata_delete_field(nx_go_logdata_t * ld, const char * field);

nx_go_str_array_t * nx_go_logdata_get_fieldnames(nx_go_logdata_t * ld);
void nx_go_array_free(nx_go_str_array_t * array);

void nx_go_log_info(const char * what);
void nx_go_log_debug(const char * what);
void nx_go_log_warning(const char * what);
void nx_go_log_error(const char * what);

void nx_go_event_add(nx_go_ctx_t * ctx);
*/
import "C"

import "unsafe"

type NxModule struct {
	ctx *C.nx_go_ctx_t
}

type NxLogdata struct {
	ld *C.nx_go_logdata_t
}


func NxModuleConv (ctx unsafe.Pointer) NxModule{
	return NxModule{ctx: (*C.nx_go_ctx_t)(ctx)}
}

func NxLogdataConv(ld unsafe.Pointer) NxLogdata {
	return NxLogdata{ld: (*C.nx_go_logdata_t)(ld)}
}

func GoStrings(argc C.int, argv **C.char) []string {

	length := int(argc)
	tmpslice := (*[1 << 30]*C.char)(unsafe.Pointer(argv))[:length:length]
	gostrings := make([]string, length)
	for i, s := range tmpslice {
		gostrings[i] = C.GoString(s)
	}
	return gostrings
}

func NxLogdataNew() NxLogdata {
	return NxLogdata{ ld: C.nx_go_logdata_new()}
}

func NxLogdataFree(ld NxLogdata) {
	C.nx_go_logdata_free(ld.ld)
}

func (ld NxLogdata) Get(field string) string {
	cName := C.CString(field)
	defer C.free(unsafe.Pointer(cName))
	cVal := C.nx_go_logdata_get(ld.ld, cName)
	defer C.free(unsafe.Pointer(cVal))
	return C.GoString(cVal)
}
func (ld NxLogdata) Set(field string, val string) {
	cName := C.CString(field)
	cVal := C.CString(val)
	defer C.free(unsafe.Pointer(cName))
	defer C.free(unsafe.Pointer(cVal))
	C.nx_go_logdata_set(ld.ld, cName, cVal)
}

func (ld NxLogdata) Delete(field string) {
	cField := C.CString(field)
	defer C.free(unsafe.Pointer(cField))
	C.nx_go_logdata_delete_field(ld.ld, cField)
}

func (ld NxLogdata) Fields() []string {
	cFields := C.nx_go_logdata_get_fieldnames(ld.ld)
	defer C.nx_go_array_free(cFields)
	return GoStrings(cFields.nelts, cFields.elts)
}


func (mod NxModule) Post(ld NxLogdata) {
	C.nx_go_logdata_post(mod.ctx, ld.ld)
}

func (mod NxModule) AddEvent() {
	C.nx_go_event_add(mod.ctx)
}

func LogInfo(what string){
	cWhat := C.CString(what)
	defer C.free(unsafe.Pointer(cWhat))
	C.nx_go_log_info(cWhat)
}

func LogDebug(what string) {
	cWhat := C.CString(what)
	defer C.free(unsafe.Pointer(cWhat))
	C.nx_go_log_debug(cWhat)
}

func LogWarn(what string) {
	cWhat := C.CString(what)
	defer C.free(unsafe.Pointer(cWhat))
	C.nx_go_log_warning(cWhat)
}

func LogError(what string) {
	cWhat := C.CString(what)
	defer C.free(unsafe.Pointer(cWhat))
	C.nx_go_log_error(cWhat)
}

